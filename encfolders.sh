#!/bin/bash

# set -x
shopt -s extglob # to use negative globs

# Set default:
METHOD=${METHOD:-7zparallel}
# METHOD=7z

usage() {
    echo "$0 -e <dir>"
    echo "$0 -d <file.7z>"
}

echoerr() { echo "$@" 1>&2; }

prog_exists() {
    if ! command -v $1 &> /dev/null
    then
        echoerr $1 cannot could not be found
        exit 1
    fi
}

read_passwd() {
        echo -n "Password: " 1>&2
        read -s P

        echo 1>&2
        echo $P
}

7zencrypt() { # Encrypt a directory

    # Sure is a dir
    local DIR="$1"
    local P="$2"

    fenc="$DIR.7z"
    rm -f "$fenc"

    echo Encrypting $f to $fenc

    7z a "$fenc" -m1=copy -mhe -mmt -p"$P" "$DIR"

    if [ $? == 0 ]
    then
        rm -rf "$DIR"
    else
        return 1
    fi
}

7zdecrypt() { # Decrypt a regular file

    # Could be any
    local FILE="$1"
    local P="$2"

    EXT="${FILE##*.}"

    if ! [ -f "$FILE" ] || ! [ "$EXT" == 7z ]
    then
        echoerr "$FILE must be a 7z file"
        return 1
    fi

    local DIR="$(dirname "$FILE")"

    echo Decrypting $FILE into $DIR
    7z x "$FILE" -o"$DIR" -mmt -p"$P"

    if [ $? == 0 ]
    then
        rm -f "$FILE"
    else
        return 1
    fi
}

7zparallelencrypt() { # encrypt each files in directory

    # Sure is a dir
    local DIR="$1"
    local P="$2"

    pushd "$DIR"  > /dev/null 

    i=0
    for f in *
    do

        # Do not encode 7z files
        EXT="${f##*.}"
        if [ "$EXT" != 7z ]
        then
            fenc="$i.7z"
            # skip if file exist
            while [ -e "$fenc" ]
            do
                i=$[$i+1]
                fenc="$i.7z"
            done

            echo "$fenc"
            echo "$f"
            echo "$P"

            i=$[$i+1]
        fi
    done \
    | parallel -N 3 '7z a {1} -m1=copy -mhe -p{3} {2} && rm -rf {2}'

    popd > /dev/null
}

7zparalleldecrypt() { # Decrypt each file in directory

    # Could be any
    local DIR="${1%/}"
    local P="$2"

    if ! [ -d "$DIR" ]
    then
        echoerr "$DIR must be folder"
        return 1
    fi

    local FILE
    for FILE in "$DIR"/*.7z
    do

        if [ -f "$FILE" ]
        then

            echo "$FILE"
            echo "$DIR"
            echo "$P"
            #echo Decrypting $FILE into $DIR
            #7z x "$FILE" -o"$DIR" -mmt -p"$P"

            #if [ $? == 0 ]
            #then
            #    rm -f "$FILE"
            #else
            #    return 1
            #fi
        fi
    done \
    | parallel -N 3 '7z x "{1}" -o"{2}" -p"{3}" && rm -rf {1}'

}

encrypt() {
    ${METHOD}encrypt "$@"
}

decrypt() {
    ${METHOD}decrypt "$@"
}


encrypt_folders() {

        DIR="${1%/}"

        P=$2

        if ! [ -d "$DIR" ]
        then
            echoerr "$DIR is not a folder file"
            exit 1
        fi

        for f in "$DIR"/*
        do

            if [ -d "$f" ]
            then

                # encrypt if there are files not encrypted
                for faux in "${f}/"!(*.7z)
                do
                    if [ -e "$faux" ]
                    then

                        # Fill pass if not defined
                        if [ -z "$P" ]
                        then
                            P="$(read_passwd)"
                        fi


                        encrypt "$f" "$P"

                        if [ $? != 0 ]
                        then
                            echoerr Cannot encrypt $f
                            exit 1
                        fi
                    fi
                    break
                done
            fi

        done

}


for prg in "7z find dirname parallel"
do
    prog_exists $prg
done

OP="$1"

case "$OP" in
    -e) # Encript Folders

        shift

        encrypt_folders "$@"

        ;;

    -d) # Decript encryped element in folder

        FILE="$2"

        if ! ls "$FILE/"*.7z > /dev/null 2>&1
        then
            echoerr "Not compresed files found"
            exit 1
        fi

        P="$(read_passwd)"

        decrypt "$FILE" "$P"

        if [ $? == 0 ]
        then
            echo -n "Press enter when finish "
            read

            DIR="$(dirname "$FILE")"
            # Encrypt all always when ends
            encrypt_folders "$DIR" "$P"

        else
            exit 1
        fi

        ;;
    *)
        usage
        exit 1
        ;;
esac




